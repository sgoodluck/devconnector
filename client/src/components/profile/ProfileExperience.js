import Moment from 'react-moment';
import PropTypes from 'prop-types';
import React from 'react';

const ProfileExperience = ({
	experience: { company, title, location, current, to, from, description }
}) => (
	<div>
		<h3 className='text-dark'>{company}</h3>
		<p>
			<Moment format='YYYY/MM/DD'>{from}</Moment> -{' '}
			{!to ? ' Present' : <Moment format='YYYY/MM/DD'>{to}</Moment>}
		</p>
		<p>
			<strong>Postion: </strong> {title}
		</p>
		<p>
			<strong>Description: </strong> {description}
		</p>
	</div>
);

ProfileExperience.propTypes = {
	experience: PropTypes.array.isRequired
};

export default ProfileExperience;
