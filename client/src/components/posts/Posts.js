import React, { Fragment, useEffect } from 'react';

import PostForm from './PostForm';
import PostItem from './PostItem';
import PropTypes from 'prop-types';
import Spinner from 'components/layout/Spinner';
import { connect } from 'react-redux';
import { getPosts } from 'actions/post';

const Posts = ({ getPosts, post: { posts, loading } }) => {
	useEffect(() => {
		getPosts();
	}, [getPosts]);

	return loading ? (
		<Spinner />
	) : (
		<Fragment>
			<h1 className='large text-primary'>Posts</h1>
			<p className='lead'>
				<i className='fas fa-user'></i>Welcome to the community!
			</p>
			<PostForm />
			<div className='posts'>
				{posts.map(post => (
					<PostItem key={post._id} post={post} />
				))}
			</div>
		</Fragment>
	);
};

Posts.propTypes = {
	getPosts: PropTypes.func.isRequired,
	post: PropTypes.object.isRequired
};

const mapStarteToProps = state => ({
	post: state.post
});
export default connect(
	mapStarteToProps,
	{ getPosts }
)(Posts);
